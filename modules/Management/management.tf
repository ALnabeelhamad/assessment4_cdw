variable nabeel {default=[],type="list"}

module "my_vault" {
  source                 = "../modules/vault"
  access_key             = ""
  secret_key             = ""
  region                 = "us-east-1"
  image_id               = "ami-009d6802948d06e52"
  security_groups        = "Nabeel"
  vpc_cidr               = "172.17.0.0/16"
  vpc_id                 = "${module.my_vault.vpc_id}"
  public_subnet_cidr     = "172.17.3.0/24"
  public_subnet_id       = "${module.my_vault.subnet_id}"
  instance_type          = "t2.micro"
  key_name               = "MiNT"
  tag                    = "MiNT"
  gateway_id             = "${module.my_vault.gateway_id}"
  # route_table_id         = "${module.my_vault.route_table_id}"
  security_group_id      = "${module.my_vault.security_group_id}"
}

module "my_jenkins" {
  source                 = "../modules/jenkins"
  access_key             = ""
  secret_key             = ""
  region                 = "us-east-1"
  image_id               = "ami-009d6802948d06e52"
  security_groups        = "Nabeel"
  vpc_cidr               = "172.17.0.0/16"
  vpc_id                 = "${module.my_jenkins.vpc_id}"
  public_subnet_cidr     = "172.17.3.0/24"
  public_subnet_id       = "${module.my_jenkins.subnet_id}"
  instance_type          = "t2.micro"
  key_name               = "MiNT"
  tag                    = "MiNT"
  gateway_id             = "${module.my_jenkins.gateway_id}"
  # route_table_id         = "${module.my_jenkins.route_table_id}"
  security_group_id      = "${module.my_jenkins.security_group_id}"
  nabeel             = ["${module.my_vault.public_dns_vault}"]
}
