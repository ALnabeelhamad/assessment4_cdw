resource "aws_iam_role" "jenkinsrole" {
  name = "mint_jenkinsrole"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "jenkins_profile" {
  name = "mint_jenkins_profile"
  role = "${aws_iam_role.jenkinsrole.name}"
}

resource "aws_iam_role_policy_attachment" "jenkins_s3" {
  role       = "${aws_iam_role.jenkinsrole.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}
resource "aws_iam_role_policy_attachment" "jenkins_ecs" {
  role       = "${aws_iam_role.jenkinsrole.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonECS_FullAccess"
}
resource "aws_iam_role_policy_attachment" "git_hub" {
  role       = "${aws_iam_role.jenkinsrole.name}"
  policy_arn = "arn:aws:iam::109964479621:policy/github_policy"
}
resource "aws_iam_role_policy_attachment" "jenkin_policy" {
  role       = "${aws_iam_role.jenkinsrole.name}"
  policy_arn = "arn:aws:iam::109964479621:policy/JenkinsPolicy"
}
resource "aws_iam_role_policy_attachment" "rds_policy" {
  role       = "${aws_iam_role.jenkinsrole.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonRDSFullAccess"
}
