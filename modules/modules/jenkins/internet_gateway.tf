# Define the internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = "${var.vpc_id}"

  tags {
    Name = "${var.tag}_internet_gateway"
  }
}
