resource "aws_instance" "jenkins" {
  # depends_on                  = ["data.template_file.haproxy"]
  ami                         = "${var.image_id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  subnet_id                   = "${var.public_subnet_id}"
  vpc_security_group_ids      = ["${var.security_group_id}"]
  iam_instance_profile        = "${aws_iam_instance_profile.jenkins_profile.name}"
  associate_public_ip_address = true
  tags {
    Name = "${var.tag}-jenkins"
  }

  # provisioner "local-exec" {
  # command = "sleep 240"
  # }

  connection {
    user         = "ec2-user"
    private_key  = "${file("~/.aws/MiNT.pem")}"
  }


  # adding jenkins hosts file
  provisioner "local-exec" {
    command = "echo [jenkinsprovisionGALAXY] > ../modules/jenkins/environments/dev/hosts"
  }

  # Save the public dns name on hosts file
  provisioner "local-exec" {
    command = "echo '${aws_instance.jenkins.public_dns}' >> ../modules/jenkins/environments/dev/hosts"
  }

  # run ansible script
  provisioner "local-exec" {
    command = "ansible-playbook -i ../modules/jenkins/environments/dev/hosts ../modules/jenkins/site.yml"
  }
}
