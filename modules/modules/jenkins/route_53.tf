data "aws_route53_zone" "selected" {
 name         = "grads.al-labs.co.uk."
 private_zone = false
}

resource "aws_route53_record" "www" {
 zone_id = "${data.aws_route53_zone.selected.zone_id}"
 name    = "www.MiNT-Jenkins.grads.al-labs.co.uk"
 type    = "CNAME"
 ttl     = "30"
 records  = ["${aws_instance.jenkins.public_dns}"]
}
