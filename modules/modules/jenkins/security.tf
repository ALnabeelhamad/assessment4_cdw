# Create security group with web and ssh access
resource "aws_security_group" "ec2" {
  name = "Nabeel"

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["77.108.144.180/32", "82.37.176.40/32"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 8080
    to_port     = 8080
    cidr_blocks = ["77.108.144.180/32", "82.37.176.40/32"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 8200
    to_port     = 8200
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
  Name = "MiNTY SG-Jenkins"
  }

  vpc_id="${var.vpc_id}"
}
