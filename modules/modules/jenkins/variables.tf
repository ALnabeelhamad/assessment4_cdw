variable "access_key" {}
variable "secret_key" {}

variable "region" {
  default = "us-east-1"
}

variable "image_id" {
  default = "ami-009d6802948d06e52"
}

variable "security_groups" {
  default = "Nabeel"
}

variable "vpc_cidr" {
  default = "172.17.0.0/16"
}

variable "vpc_id" {}

variable "public_subnet_cidr" {
  description = "CIDR for the public subnet"
  default = "172.17.3.0/24"
}

variable "public_subnet_id" {}

variable "instance_type" {
  description = "size of instance used"
  default = "t2.micro"
}

variable "key_name" {
  description = "private key pair name as displayed in aws site"
  default = "MiNT"
}

variable "tag" {
  description = "associated tag"
  default = "MiNT_MANGMT"
}

variable "gateway_id" {}

# variable "route_table_id" {}

variable "security_group_id" {}

variable nabeel {default=[],type="list"}
