resource "aws_instance" "vault" {
  # depends_on                  = ["data.template_file.xxx"]
  ami                         = "${var.image_id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  subnet_id                   = "${var.public_subnet_id}"
  vpc_security_group_ids      = ["${aws_security_group.ec2.id}"]
  associate_public_ip_address = true
  tags {
    Name = "${var.tag}-vault"
  }

  connection {
    user         = "ec2-user"
    private_key  = "${file("~/.aws/MiNT.pem")}"
  }

  # adding vault on top
  provisioner "local-exec" {
    command = "echo [vaultprovision] >> ../modules/vault/environments/dev/hosts"
  }

  # Save the public IP for testing
  provisioner "local-exec" {
    command = "echo ${aws_instance.vault.public_dns} >> ../modules/vault/environments/dev/hosts"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.vault.public_ip} > ../modules/vault/ec2vltinstip"
  }

  # provisioner "local-exec" {
  # command = "sleep 5"
  # }

  # run ansible script
  provisioner "local-exec" {
    command = "ansible-playbook -i ../modules/vault/environments/dev/hosts ../modules/vault/site.yml"
  }
}

output "public_dns_vault" {
  value = "${aws_instance.vault.public_dns}"
}
